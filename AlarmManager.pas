﻿namespace Essy.Tools;

interface

uses
  System.Collections.Generic,
  System.Media;

type
  AlarmManager = public static class
  private
    fAlarmList: List<Alarm> := new List<Alarm>();
    fAlarmPlayer: SoundPlayer := new SoundPlayer();
    method fUpdatePlayer;
  protected
  public
    method AddAlarm(aAlarmID, aAlarmWaveFile: String);
    method UpdateAlarm(aAlarmID, aAlarmWaveFile: String);
    method RemoveAlarm(aAlarmID: String);
    method RemoveAllAlarms;
    method PlayOnce(aAlarmWavFile: String);
    method ContainsAlarm(aAlarmID: String):Boolean;
  end;

  Alarm = assembly class
  private
  public
    property ID: String;
    property WaveFile: String;
    property Isplaying: Boolean := false;
  end;

implementation

uses 
  System.IO;

method AlarmManager.AddAlarm(aAlarmID, aAlarmWaveFile: String);
begin
  var alarm := self.fAlarmList.Find(a -> a.ID = aAlarmID);
  if not assigned(alarm) then
  begin
    self.fAlarmList.Add(new Alarm(ID := aAlarmID, WaveFile := aAlarmWaveFile));
    fUpdatePlayer();
  end;  
end;

method AlarmManager.RemoveAlarm(aAlarmID: String);
begin
  if self.fAlarmList.RemoveAll(a -> a.ID = aAlarmID) > 0 then fUpdatePlayer();
end;

method AlarmManager.UpdateAlarm(aAlarmID, aAlarmWaveFile: String);
begin
  var alarm := self.fAlarmList.Find(a -> a.ID = aAlarmID);
  if assigned(alarm) then
  begin
    alarm.WaveFile := aAlarmWaveFile;
    alarm.Isplaying := false;
    fUpdatePlayer();
  end;
end;

method AlarmManager.PlayOnce(aAlarmWavFile: String);
begin
  if ((String.IsNullOrEmpty(aAlarmWavFile)) or (not File.Exists(aAlarmWavFile))) then exit; 
  self.fAlarmPlayer.SoundLocation := aAlarmWavFile;
  self.fAlarmPlayer.Play();
end;

method AlarmManager.RemoveAllAlarms;
begin
   self.fAlarmList.Clear();
  fUpdatePlayer();
end;

method AlarmManager.fUpdatePlayer;
begin
  if self.fAlarmList.Count > 0 then
  begin
    for each alarm in self.fAlarmList index indx do
    begin
      if indx = 0 then
      begin
        if not alarm.Isplaying and File.Exists(alarm.WaveFile) then 
        begin
          self.fAlarmPlayer.SoundLocation := alarm.WaveFile;
          self.fAlarmPlayer.PlayLooping();
          alarm.Isplaying := true;
        end;
      end
      else
      begin
        alarm.Isplaying := false;
      end;
    end;
  end
  else
  begin
    self.fAlarmPlayer.Stop();
  end;
end;

method AlarmManager.ContainsAlarm(aAlarmID: String): Boolean;
begin
  var alarm := self.fAlarmList.Find(a -> a.ID = aAlarmID);
  result := assigned(alarm);
end;

end.
